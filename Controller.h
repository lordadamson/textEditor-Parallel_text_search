#pragma once
#include <string>
#include <vector>
#include <map>

using namespace std;

class Controller
{
	string m_keyword;
	string m_file_content;
	char m_keyword_to_find;
	vector<int> m_positions;
	map<short, vector<int>*> m_char_to_positions;

	void initialize_char_to_positions();
	void index_file();

public:
	Controller(string file_content);
	vector<int> search(string keyword);
	vector<int> search(string keyword, vector<int> p_positions);
};
