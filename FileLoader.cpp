#include <omp.h>
#include <iostream>
#include <fstream>
#include <string>
#include "FileLoader.h"

FileLoader::FileLoader()
{
	file_path = "test.txt";
	ifstream file_stream(file_path);
	if(file_stream.fail())
		data = "";
	data = string((std::istreambuf_iterator<char>(file_stream)),
				  (std::istreambuf_iterator<char>()));

	file_stream.close();
}
