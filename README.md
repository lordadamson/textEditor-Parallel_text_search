# textEditor-Parallel_text_search

A simple program that searches in a given text.

[Video explaining the project](https://www.youtube.com/watch?v=pce2BDSnkfI)

## Dependencies
- Qt 5.5+
- OpenMP

## Usage
Using qtcreator open the .pro file, compile and run. Place a text file named `test.txt` at the location of the compiled binary (Usually the debug folder in your project directory) with the content you'd like to search in.
