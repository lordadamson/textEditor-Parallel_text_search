#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <string>
#include <vector>

#include "Controller.h"
#include "FileLoader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
	Ui::MainWindow *ui;

	QTextEdit *ui_searchBar;
	QTextEdit *ui_textEdit;
	QString keyword;
	QString file_content;

	vector<int> keyword_positions;

	Controller* controller;
	FileLoader* loader;

	void highlightKeyword();
private slots:
	void on_searchBar_textChanged();
};

#endif // MAINWINDOW_H
