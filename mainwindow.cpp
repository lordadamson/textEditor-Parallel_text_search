#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui_searchBar = findChild<QTextEdit*>("searchBar");
	ui_textEdit = findChild<QTextEdit*>("textEdit");
	loader = new FileLoader();
	file_content = QString::fromStdString(loader->data);
	ui_textEdit->setText(file_content);
	controller = new Controller(file_content.toStdString());
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_searchBar_textChanged()
{
	QString tmp_keyword = keyword;
	keyword = ui_searchBar->toPlainText();

	// workaround for a bug in qt. qt fires the signal twice
	// when backspace is pressed leading to the search running twice
	// for no good reason
	if(tmp_keyword == keyword)
	{
		keyword_positions.clear();
		return;
	}

	if (keyword_positions.size() == 0)
		keyword_positions = controller->search(keyword.toStdString());
	else // continue searching on the values you've already found instead of starting the search all over again
		keyword_positions = controller->search(keyword.toStdString(), keyword_positions);
	highlightKeyword();
}

void MainWindow::highlightKeyword()
{
	QList<QTextEdit::ExtraSelection> extraSelections;
	QTextEdit::ExtraSelection selection;
	QColor lineColor = QColor(Qt::yellow);

	QTextCursor cursor(ui_textEdit->document());

	#pragma omp parallel
	{
		#pragma omp for
		//highlight found words
		for (auto keyword_position : keyword_positions)
		{
			cout << keyword_position << endl;
			cursor.setPosition(keyword_position, QTextCursor::MoveAnchor);
			cursor.setPosition(keyword_position + keyword.size(), QTextCursor::KeepAnchor);
			selection.format.setBackground(lineColor);
			//selection.format.setProperty(QTextFormat::FullWidthSelection, true);
			selection.cursor = cursor;
			//selection.cursor.clearSelection();
			extraSelections.append(selection);
		}
	}

	ui_textEdit->setExtraSelections(extraSelections);

}


