#-------------------------------------------------
#
# Project created by QtCreator 2016-12-24T21:19:26
#
#-------------------------------------------------

QT       += core gui
CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = text_search
TEMPLATE = app
LIBS += -fopenmp

SOURCES += main.cpp\
        mainwindow.cpp \
        Controller.cpp \
        FileLoader.cpp

HEADERS  += mainwindow.h \
        Controller.h \
        FileLoader.h

FORMS    += mainwindow.ui

DISTFILES += \
    test.txt
