#include "Controller.h"
#include <omp.h>

Controller::Controller(string file_content)
{
	m_file_content = file_content;
	initialize_char_to_positions();
	index_file();
}


vector<int> Controller::search(string keyword)
{
	m_keyword = keyword;

	
	vector<int> founds;
	vector<int>* positions = m_char_to_positions[short(m_keyword[0])];

#pragma omp parallel
{
		bool found = true;
#pragma omp for
	for (int i = 0; i < positions->size(); i++)
	{
		int position = positions->at(i);
		int k;
		for (int j = position, k = 0; j < position + m_keyword.size(); j++, k++)
		{
			if (m_file_content[j] != m_keyword[k])
			{
				found = false;
				break;
			}
		}
		if (found)
			founds.push_back(position);
		found = true;
	}
}


	return founds;
}

vector<int> Controller::search(string keyword, vector<int> p_positions)
{
	m_keyword = keyword;


	vector<int> founds;
	vector<int>* positions = &p_positions;

#pragma omp parallel
{
		bool found = true;
#pragma omp for
	for (int i = 0; i < positions->size(); i++)
	{
		int position = positions->at(i);
		int k;
		for (int j = position, k = 0; j < position + m_keyword.size(); j++, k++)
		{
			if (m_file_content[j] != m_keyword[k])
			{
				found = false;
				break;
			}
		}
		if (found)
			founds.push_back(position);
		found = true;
	}
}


	return founds;
}

void Controller::initialize_char_to_positions()
{
#pragma omp parallel
{
	#pragma omp for
	// 128 is the ascii table size
	for (short i = 0; i < 128; i++)
		m_char_to_positions.insert( pair< short, vector<int>* >( i, new vector<int> ) );
}
}

void Controller::index_file()
{
#pragma omp parallel
{
	#pragma omp for
	for (int i = 0; i < m_file_content.size(); i++)
		m_char_to_positions[int(m_file_content[i])]->push_back(i);
}
}
